//
//  CardGameViewController.m
//  Matchismo
//
//  Created by CS193p Instructor.
//  Copyright (c) 2013 Stanford University.
//  All rights reserved.
//

#import "CardGameViewController.h"
#import "GameResult.h"

@interface CardGameViewController () <UICollectionViewDataSource>
@property (strong, nonatomic) CardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) GameResult *gameResult;
@property (weak, nonatomic) IBOutlet UICollectionView *cardCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *flippedCardsLabel;
@end

@implementation CardGameViewController

#pragma mark - Properties

- (GameResult *)gameResult
{
    if (!_gameResult) _gameResult = [[GameResult alloc] init];
    return _gameResult;
}

- (NSInteger)gameMode
{
    return 2;
}

- (CardMatchingGame *)game
{
    if (!_game) {
        _deck = [self createDeck];
        _game = [[CardMatchingGame alloc] initWithCardCount:self.startingCardCount
                                                          usingDeck:_deck];
        _game.numCardsMatch = self.gameMode;
    }
    return _game;
}

- (Deck *)createDeck { return nil; } // abstract

- (NSString *)cardIdentifier { return nil; } // abstract

- (BOOL)addCardsToGame:(NSInteger)numberOfCards
{
    if (_deck) {
        NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
        for (int i = 1; i <= numberOfCards; i++) {
            Card *card = [_deck drawRandomCard];
            if (card) {
                NSUInteger index = [self.game addCard:card];
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
                [indexPaths addObject:indexPath];
            }
        }
        [self.cardCollectionView insertItemsAtIndexPaths:indexPaths];
        [self.cardCollectionView scrollToItemAtIndexPath:indexPaths[0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
        if (indexPaths.count < numberOfCards) {
            return NO;
        }
    } else {
        return NO;
    }
    
    return YES;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return [self.game cardsInGameCount];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[self cardIdentifier] forIndexPath:indexPath];
    Card *card = [self.game cardAtIndex:indexPath.item];
    [self updateCell:cell usingCard:card animate:NO];
    return cell;
}

- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card animate:(BOOL)animate
{
    // abstract
}

#pragma mark - Updating the UI

- (void)updateUI:(NSIndexPath *)targetIndexPath
{
    for (UICollectionViewCell *cell in [self.cardCollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.cardCollectionView indexPathForCell:cell];
        Card *card = [self.game cardAtIndex:indexPath.item];
        [self updateCell:cell usingCard:card animate:(targetIndexPath.item == indexPath.item)];
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
    [self updateInfoLabel:self.flippedCardsLabel forGame:self.game];
}

- (void)updateInfoLabel:(UILabel *)label forGame:(CardMatchingGame *)game {
    NSDictionary *lastFlip = [game lastFlip];
    NSString *infoLabelText = @"Turn a card";
    NSArray *cardsInString = nil;
    if (lastFlip) {
        if ([lastFlip objectForKey:@"points"] && [lastFlip objectForKey:@"flippedCards"]) {
            NSArray *cardContents = [lastFlip[@"flippedCards"] valueForKey:@"contents"];
            cardsInString = lastFlip[@"flippedCards"];
            int points = [lastFlip[@"points"] integerValue];
            if (points > 0) {
                infoLabelText = [NSString stringWithFormat:@"Matched %@ for %d points",
                                 [cardContents componentsJoinedByString:@" & "],
                                 points
                                 ];
            } else {
                infoLabelText = [NSString stringWithFormat:@"%@ don't match! %d point penalty!",
                                 [cardContents componentsJoinedByString:@" & "],
                                 -points
                                 ];
            }
        } else if([lastFlip objectForKey:@"card"]) {
            Card *flippedCard = lastFlip[@"card"];
            infoLabelText = [NSString stringWithFormat:@"Flipped up %@",
                             flippedCard.contents];
            cardsInString = @[flippedCard];
        }
    }
    label.text = infoLabelText;
}

- (void)removeUnplayableCards
{
    NSUInteger cardsCount = [self.game cardsInGameCount];
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    NSMutableIndexSet *cardsIndexSet = [NSMutableIndexSet indexSet];
    for (NSUInteger i = 0; i < cardsCount; i++) {
        Card *card = [self.game cardAtIndex:i];
        if (card.isUnplayable) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            [cardsIndexSet addIndex:i];
            [indexPaths addObject:indexPath];
        }
    }
    [self.game removeCardsAtIndexes:cardsIndexSet];
    [self.cardCollectionView deleteItemsAtIndexPaths:indexPaths];
}

#define pragma mark - Target/Action/

- (void)startOver {
    self.game = nil;
    self.gameResult = nil;
    [self.cardCollectionView reloadData];
    [self updateUI:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        [self startOver];
    }
}

- (IBAction)deal
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Deal"
                                                    message:@"Are you sure you want to re-deal the game?"
                                                   delegate:self
                                          cancelButtonTitle:@"Yes"
                                          otherButtonTitles:@"No",
                          nil];
    [alert show];
}

- (void)processFlipCardAtIndexPath:(NSIndexPath *)indexPath
{
    // abstract
}

- (IBAction)flipCard:(UITapGestureRecognizer *)gesture
{
    CGPoint tapLocation = [gesture locationInView:self.cardCollectionView];
    NSIndexPath *indexPath = [self.cardCollectionView indexPathForItemAtPoint:tapLocation];
    if (indexPath) {
        [self.game flipCardAtIndex:indexPath.item];
        [self processFlipCardAtIndexPath:indexPath];
        [self updateUI:indexPath];
        self.gameResult.score = self.game.score;
    }
}

@end
