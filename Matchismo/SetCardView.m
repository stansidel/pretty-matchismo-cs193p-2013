//
//  SetCardView.m
//  Matchismo
//
//  Created by Stan on 07.08.15.
//  Copyright © 2015 Stanford University. All rights reserved.
//

#import "SetCardView.h"

@implementation SetCardView

- (void)setSymbol:(NSInteger)symbol
{
    _symbol = symbol;
    [self setNeedsDisplay];
}

- (void)setShading:(NSInteger)shading
{
    _shading = shading;
    [self setNeedsDisplay];
}

- (void)setColor:(NSInteger)color
{
    _color = color;
    [self setNeedsDisplay];
}

- (void)setNumber:(NSInteger)number
{
    _number = number;
    [self setNeedsDisplay];
}

#pragma mark - Drawing

- (CGFloat)cornerOffset
{
    if (!_cornerOffset) _cornerOffset = 15.0;
    return _cornerOffset;
}

- (CGFloat)cornerRadius
{
    if (!_cornerRadius) _cornerRadius = 7.0;
    return _cornerRadius;
}

- (void)drawRect:(CGRect)rect {
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.cornerRadius];
    
    [roundedRect addClip];
    
    [[UIColor grayColor] setStroke];
    [roundedRect stroke];
    
    UIColor *fillColor = nil;
    if (self.grayed) {
        fillColor = [UIColor grayColor];
    } else {
        fillColor = [UIColor whiteColor];
    }
    [fillColor setFill];
    UIRectFill(rect);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self drawShapes:context inRect:rect];
    
}

#define SHAPE_RATIO 1.78 // 16:9
- (CGFloat)minMargin
{
    if (!_minMargin) _minMargin = 5.0;
    return _minMargin;
}

- (void)drawShapes:(CGContextRef)context inRect:(CGRect)rect
{
    UIGraphicsPushContext(context);
    
    UIColor *color = [self getColor:self.color];
    [color setStroke];
    
    UIColor *fillColor = [color colorWithAlphaComponent:[self getAlpha:self.shading]];
    [fillColor setFill];
    
    CGRect imagesRect = CGRectInset(rect, self.cornerOffset, self.cornerOffset);
    CGFloat imageHeight = (imagesRect.size.height - (self.number - 1) * self.minMargin) / self.number;
    CGRect imageRect = CGRectMake(imagesRect.origin.x, imagesRect.origin.y,
                                  imagesRect.size.width, imageHeight);
    for (int i = 1; i <= self.number; i++) {
        imageRect.origin.y = imagesRect.origin.y + (imageHeight + self.minMargin) * (i - 1);
        [self drawShapeInRect:imageRect];
    }
    
    UIGraphicsPopContext();
}

- (UIColor *)getColor:(NSInteger)colorCode
{
    return @[[UIColor redColor], [UIColor redColor], [UIColor greenColor], [UIColor purpleColor]][colorCode];
}

- (CGFloat)getAlpha:(NSInteger)shadingCode
{
    if (shadingCode == 1) {
        return 1;
    } else if (shadingCode == 2) {
        return 0.3;
    } else {
        return 0;
    }
}

- (void)drawShapeInRect:(CGRect)rect
{
    CGRect shapeRect = rect;
    
    float shapeRatio = shapeRect.size.width / shapeRect.size.height;
    if (shapeRatio > SHAPE_RATIO) {
        // Taller than needed
        CGFloat requiredWidth = shapeRect.size.height * SHAPE_RATIO;
        shapeRect.origin.x += (shapeRect.size.width - requiredWidth) / 2;
        shapeRect.size.width = requiredWidth;
    } else if (shapeRatio < SHAPE_RATIO) {
        // Wider than needed
        CGFloat requiredHeight = shapeRect.size.width / SHAPE_RATIO;
        shapeRect.origin.y += (shapeRect.size.height - requiredHeight) / 2;
        shapeRect.size.height = requiredHeight;
    }
    
    if (shapeRect.size.width > 0 && shapeRect.size.height > 0) {
        if (self.symbol == 1) {
            [self drawOvalInRect:shapeRect];
        } else if (self.symbol == 2) {
            [self drawSquiggleInRect:shapeRect];
        } else if (self.symbol == 3) {
            [self drawDiamondInRect:shapeRect];
        }
    }
}

- (void)drawOvalInRect:(CGRect)rect
{
    UIBezierPath *ovalPath = [UIBezierPath bezierPathWithOvalInRect:rect];
    [ovalPath fill];
    [ovalPath stroke];
}

- (void)drawSquiggleInRect:(CGRect)rect
{
    CGFloat heightShift = rect.size.height * 0.2;
    CGFloat widthShift = rect.size.width * 0.1;
    
    CGPoint topLeftPoint = CGPointMake(rect.origin.x + widthShift, rect.origin.y + heightShift);
    CGPoint topRightPoint = CGPointMake(topLeftPoint.x + rect.size.width - widthShift * 2, topLeftPoint.y);
    CGPoint bottomRightPoint = CGPointMake(topRightPoint.x, rect.origin.y + rect.size.height - 2 * heightShift);
    CGPoint bottomLeftPoint = CGPointMake(topLeftPoint.x, bottomRightPoint.y);
    
    CGFloat curveExtent = 1.5;
    CGPoint controlPointTop1 = CGPointMake(topLeftPoint.x + (topRightPoint.x - topLeftPoint.x) / 2, topRightPoint.y - curveExtent * heightShift);
    CGPoint controlPointTop2 = CGPointMake(topLeftPoint.x + (topRightPoint.x - topLeftPoint.x) / 2, topLeftPoint.y + curveExtent * heightShift);
    CGPoint controlPointBottom1 = CGPointMake(controlPointTop1.x, bottomRightPoint.y + curveExtent * heightShift);
    CGPoint controlPointBottom2 = CGPointMake(controlPointTop2.x, bottomLeftPoint.y - curveExtent * heightShift);
    
    CGFloat quadCurveExtent = 1.5;
    CGPoint controlPointMidRight = CGPointMake(topRightPoint.x + quadCurveExtent * widthShift, topRightPoint.y + (bottomRightPoint.y - topRightPoint.y) / 2);
    CGPoint controlPointMidLeft = CGPointMake(topLeftPoint.x - quadCurveExtent * widthShift, topLeftPoint.y + (bottomLeftPoint.y - topLeftPoint.y) / 2);
    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    UIGraphicsPushContext(context);
//    UIBezierPath *controlLinesPath = [[UIBezierPath alloc] init];
//    [controlLinesPath moveToPoint:topLeftPoint];
//    [controlLinesPath addLineToPoint:controlPointTop1];
//    [controlLinesPath moveToPoint:topRightPoint];
//    [controlLinesPath addLineToPoint:controlPointTop2];
//    [controlLinesPath moveToPoint:bottomRightPoint];
//    [controlLinesPath addLineToPoint:controlPointBottom1];
//    [controlLinesPath moveToPoint:bottomLeftPoint];
//    [controlLinesPath addLineToPoint:controlPointBottom2];
//    
//    [controlLinesPath moveToPoint:topRightPoint];
//    [controlLinesPath addLineToPoint:controlPointMidRight];
//    [controlLinesPath addLineToPoint:bottomRightPoint];
//    
//    [controlLinesPath moveToPoint:bottomLeftPoint];
//    [controlLinesPath addLineToPoint:controlPointMidLeft];
//    [controlLinesPath addLineToPoint:topLeftPoint];
//    
//    [[UIColor redColor] setStroke];
//    [controlLinesPath stroke];
//    UIGraphicsPopContext();
    
    UIBezierPath *squigglePath = [[UIBezierPath alloc] init];
    [squigglePath moveToPoint:topLeftPoint];
    [squigglePath addCurveToPoint:topRightPoint
                    controlPoint1: controlPointTop1
                    controlPoint2: controlPointTop2];
    [squigglePath addQuadCurveToPoint:bottomRightPoint controlPoint: controlPointMidRight];
    [squigglePath addCurveToPoint:bottomLeftPoint
                    controlPoint1: controlPointBottom1
                    controlPoint2: controlPointBottom2];
    [squigglePath addQuadCurveToPoint:topLeftPoint controlPoint: controlPointMidLeft];
    [squigglePath closePath];
    
    [squigglePath fill]; [squigglePath stroke];
}

- (void)drawDiamondInRect:(CGRect)rect
{
    UIBezierPath *diamondPath = [[UIBezierPath alloc] init];
    [diamondPath moveToPoint:CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y)];
    [diamondPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width,
                                            rect.origin.y + rect.size.height / 2)];
    [diamondPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width / 2,
                                            rect.origin.y + rect.size.height)];
    [diamondPath addLineToPoint:CGPointMake(rect.origin.x,
                                            rect.origin.y + rect.size.height / 2)];
    [diamondPath closePath];
    
    [diamondPath fill]; [diamondPath stroke];
}


@end
