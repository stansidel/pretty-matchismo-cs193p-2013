//
//  SetCard.h
//  Matchismo
//
//  Created by Stanislav Sidelnikov on 27/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "Card.h"

@interface SetCard : Card

@property (nonatomic) NSInteger symbol;
@property (nonatomic) NSInteger number;
@property (nonatomic) NSInteger shading;
@property (nonatomic) NSInteger color;

+ (NSInteger) numSymbols;
+ (NSInteger) numShadings;
+ (NSInteger) numColors;
+ (NSInteger) maxNumber;

@end
