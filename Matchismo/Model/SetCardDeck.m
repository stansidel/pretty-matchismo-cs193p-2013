//
//  SetCardDeck.m
//  Matchismo
//
//  Created by Stanislav Sidelnikov on 27/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "SetCardDeck.h"
#import "SetCard.h"

@implementation SetCardDeck

- (instancetype) init
{
    self = [super init];
    
    if (self) {
        for (NSInteger symbol = 1; symbol <= [SetCard numColors]; symbol++) {
            for (NSInteger color = 1; color <= [SetCard numColors]; color++) {
                for (NSInteger shading = 1; shading <= [SetCard numShadings]; shading++) {
                    for (NSInteger n = 1; n <= [SetCard maxNumber]; n++) {
                        SetCard *card = [[SetCard alloc] init];
                        card.symbol = symbol;
                        card.color = color;
                        card.shading = shading;
                        card.number = n;
                        [self addCard:card];
                    }
                }
            }
        }
    }
    
    return self;
}

@end
