//
//  CardGameViewController.h
//  Matchismo
//
//  Created by CS193p Instructor.
//  Copyright (c) 2013 Stanford University.
//  All rights reserved.
//
//  This class is abstract.  See subclasser instructions below.

#import <UIKit/UIKit.h>
#import "Deck.h"
#import "CardMatchingGame.h"

@interface CardGameViewController : UIViewController

- (void)removeUnplayableCards;
- (BOOL)addCardsToGame:(NSInteger)numberOfCards;
- (void)processFlipCardAtIndexPath:(NSIndexPath *)indexPath; // abstract
- (void)updateInfoLabel:(UILabel *)label forGame:(CardMatchingGame *)game;

@property (readonly, nonatomic) NSInteger gameMode;
@property (readonly, strong, nonatomic) Deck *deck;

// all of the following methods must be overriden by concrete subclasses
- (Deck *)createDeck; // abstract
@property (readonly, nonatomic) NSUInteger startingCardCount; // abstract
- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card animate:(BOOL)animate; // abstract
- (NSString *)cardIdentifier; // abstract

@end
