//
//  SetCardView.h
//  Matchismo
//
//  Created by Stan on 07.08.15.
//  Copyright © 2015 Stanford University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetCardView : UIView

@property (nonatomic) NSInteger symbol;
@property (nonatomic) NSInteger shading;
@property (nonatomic) NSInteger color;
@property (nonatomic) NSInteger number;

@property (nonatomic) BOOL grayed;

@property (nonatomic) CGFloat cornerRadius;
@property (nonatomic) CGFloat cornerOffset;
@property (nonatomic) CGFloat minMargin;

@end
