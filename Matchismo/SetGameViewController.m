//
//  SetGameViewController.m
//  Matchismo
//
//  Created by Stan on 07.08.15.
//  Copyright © 2015 Stanford University. All rights reserved.
//

#import "SetGameViewController.h"
#import "SetCardDeck.h"
#import "SetCard.h"
#import "SetCardView.h"
#import "SetCardCollectionViewCell.h"

@interface SetGameViewController ()
@property (weak, nonatomic) IBOutlet UIButton *addCardsButton;

@end

@implementation SetGameViewController

- (Deck *)createDeck
{
    self.addCardsButton.enabled = YES;
    return [[SetCardDeck alloc] init];
}

- (NSUInteger) startingCardCount
{
    return 12;
}

- (NSInteger) gameMode
{
    return 3;
}

- (NSString *)cardIdentifier
{
    return @"SetCard";
}

- (void)updateSetCardView:(SetCardView *)setCardView withSetCard:(SetCard *)setCard
{
    setCardView.shading = setCard.shading;
    setCardView.color = setCard.color;
    setCardView.symbol = setCard.symbol;
    setCardView.number = setCard.number;
    setCardView.grayed = setCard.isFaceUp;
}

- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card animate:(BOOL)animate
{
    if ([cell isKindOfClass:[SetCardCollectionViewCell class]]) {
        SetCardView *setCardView = ((SetCardCollectionViewCell *)cell).setCardView;
        if ([card isKindOfClass:[SetCard class]]) {
            SetCard *setCard = (SetCard *)card;
            if (!setCard.isUnplayable) {
                [self updateSetCardView:setCardView withSetCard:setCard];
            }
        }
    }
}

- (void)processFlipCardAtIndexPath:(NSIndexPath *)indexPath
{
    [self removeUnplayableCards];
}

- (IBAction)addMoreCards:(UIButton *)sender {
    BOOL successful = [self addCardsToGame:3];
    
    if (!successful) {
        UIAlertView *feedback = [[UIAlertView alloc] initWithTitle:@"No more cards"
                                                           message:[NSString stringWithFormat:@"There are no more cards in the deck"]
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [feedback show];
        self.addCardsButton.enabled = NO;
    }
}

- (void)updateInfoLabel:(UILabel *)label forGame:(CardMatchingGame *)game
{
    NSDictionary *lastFlip = [game lastFlip];
    NSString *infoLabelText = @"Turn a card";
    for (UIView *view in label.subviews) {
        [view removeFromSuperview];
    }
    if (lastFlip) {
        if ([lastFlip objectForKey:@"flippedCards"]) {
            NSArray *flippedCards = [lastFlip objectForKey:@"flippedCards"];
            if ([lastFlip objectForKey:@"points"]) {
                NSArray *cardsInString = lastFlip[@"flippedCards"];
                int points = [lastFlip[@"points"] integerValue];
                if (points > 0) {
                    [self updateUILabel:label withText:@"Matched: " andSetCards:cardsInString];
                } else {
                    [self updateUILabel:label withText:@"Mismatch: " andSetCards:cardsInString];
                }
            } else if (flippedCards && flippedCards.count > 0) {
                [self updateUILabel:label withText:@"Flipped up" andSetCards:flippedCards];
            } else if ([lastFlip objectForKey:@"card"]) {
                flippedCards = @[lastFlip[@"card"]];
                [self updateUILabel:label withText:@"Flipped up" andSetCards:flippedCards];
            } else {
                label.text = infoLabelText;
            }
        }
        [label setNeedsDisplay];
    } else {
        label.text = infoLabelText;
    }
}

#define LASTFLIP_CARD_OFFSET_FACTOR 1.2
- (void)updateUILabel:(UILabel *)label withText:(NSString *)text andSetCards:(NSArray *)setCards
{
    if ([setCards count]) {
        label.text = text;
        CGFloat x = [label.text sizeWithFont:label.font].width;
        
        for (SetCard *setCard in setCards) {
            [self addSetCard:setCard toView:label atX:x];
            x += label.bounds.size.height * LASTFLIP_CARD_OFFSET_FACTOR;
        }
    } else label.text = @"";
}

- (void)addSetCard:(SetCard *)setCard toView:(UIView *)view atX:(CGFloat)x
{
    CGFloat height = view.bounds.size.height;
    SetCardView *setCardView = [[SetCardView alloc] initWithFrame:CGRectMake(x, 0, height, height)];
    setCardView.color = setCard.color;
    setCardView.symbol = setCard.symbol;
    setCardView.shading = setCard.shading;
    setCardView.number = setCard.number;
    setCardView.cornerOffset = 3.5;
    setCardView.cornerRadius = 1.5;
    setCardView.minMargin = 1.0;
    setCardView.backgroundColor = [UIColor clearColor];
    [view addSubview:setCardView];
}

@end
