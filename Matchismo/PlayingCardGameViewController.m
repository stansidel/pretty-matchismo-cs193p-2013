//
//  PlayingCardGameViewController.m
//  Matchismo
//
//  Created by CS193p Instructor.
//  Copyright (c) 2013 Stanford University.
//  All rights reserved.
//

#import "PlayingCardGameViewController.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h"
#import "PlayingCardCollectionViewCell.h"

@implementation PlayingCardGameViewController

- (Deck *)createDeck
{
    return [[PlayingCardDeck alloc] init];
}

- (NSUInteger)startingCardCount
{
    return 22;
}

- (void)updatePlayintCardView:(PlayingCardView *)playingCardView withPlayingCard:(PlayingCard *)playingCard
{
    playingCardView.rank = playingCard.rank;
    playingCardView.suit = playingCard.suit;
    playingCardView.faceUp = playingCard.isFaceUp;
    playingCardView.alpha = playingCard.isUnplayable ? 0.3 : 1.0;
}

- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card animate:(BOOL)animate
{
    if ([cell isKindOfClass:[PlayingCardCollectionViewCell class]]) {
        PlayingCardView *playingCardView = ((PlayingCardCollectionViewCell *)cell).playingCardView;
        if ([card isKindOfClass:[PlayingCard class]]) {
            PlayingCard *playingCard = (PlayingCard *)card;
            if (animate) {
                UIViewAnimationOptions *options = playingCard.isFaceUp ? UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight;
                [UIView transitionWithView:playingCardView
                                  duration:0.2
                                   options:options
                                animations:^{
                                    [self updatePlayintCardView:playingCardView withPlayingCard:playingCard];
                                }
                                completion:NULL];
            } else {
                [self updatePlayintCardView:playingCardView withPlayingCard:playingCard];
            }
        }
    }
}

- (NSString *)cardIdentifier
{
    return @"PlayingCard";
}

@end
