//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by stan on 23/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()
@property (strong, nonatomic) NSMutableArray *cards;
@property (nonatomic) int score;
@property (strong, nonatomic) NSMutableArray *flipsLog;
@property (nonatomic) BOOL gameStarted;
@end

@implementation CardMatchingGame

- (NSMutableArray *) cards {
    if (!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (NSInteger)cardsInGameCount
{
    return self.cards.count;
}

- (NSUInteger)addCard:(Card *)card
{
    if (card) {
        [self.cards addObject:card];
        return self.cards.count - 1;
    } else {
        return nil;
    }
}

- (NSMutableArray *)flipsLog {
    if (!_flipsLog) _flipsLog = [[NSMutableArray alloc] init];
    return _flipsLog;
}

- (void)logCardUnflip:(NSArray *)otherCards {
    [self.flipsLog addObject:@{@"flippedCards": otherCards}];
}

- (void)logCardFlip:(Card *)card andOtherCards:(NSArray *)otherCards {
    [self.flipsLog addObject:@{@"card": card, @"flippedCards": [otherCards arrayByAddingObject:card]}];
}

- (void)logCardFlip:(Card *)card withPoints:(int)points andOtherCards:(NSArray *)otherCards {
    [self.flipsLog addObject:@{
                               @"card": card,
                               @"points": @(points),
                               @"flippedCards": [otherCards arrayByAddingObject:card]
                               }
     ];
}

- (NSDictionary *)lastFlip {
    return [self.flipsLog lastObject];
}

- (id)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck {
    self = [super init];
    
    if (self) {
        for (int i = 0; i < count; i++) {
            Card *card = [deck drawRandomCard];
            if (!card) {
                self = nil;
            } else {
                self.cards[i] = card;
            }
        }
    }
    
    self.numCardsMatch = 2;
    
    return self;
}

- (Card *)cardAtIndex:(NSUInteger)index {
    return (index < self.cards.count) ? self.cards[index] : nil;
}

#define FLIP_COST 1
#define MISMATCH_PENALTY 2
#define MATCH_BONUS 4

- (void)flipCardAtIndex:(NSUInteger)index {
    self.gameStarted = YES;
    Card *card = [self cardAtIndex:index];
    
    if (!card.isUnplayable) {
        NSMutableArray *selectedCards = [[NSMutableArray alloc] init];
        for (Card *otherCard in self.cards) {
            if (otherCard.isFaceUp && !otherCard.isUnplayable && otherCard != card) {
                [selectedCards addObject:otherCard];
            }
        }
        if (!card.isFaceUp) {
            if ([selectedCards count] + 1 == self.numCardsMatch) {
                int matchScore = (int)[card match:selectedCards];
                int points = 0;

                if (matchScore) {
                    for (Card *otherCard in selectedCards) {
                        otherCard.unplayable = YES;
                    }
                    card.unplayable = YES;
                    points = matchScore * MATCH_BONUS;
                } else {
                    for (Card *otherCard in selectedCards) {
                        otherCard.faceUp = NO;
                    }
                    card.faceUp = YES;
                    points = -MISMATCH_PENALTY;
                }
                self.score += points;
                [self logCardFlip:card withPoints:points andOtherCards:selectedCards];
            } else {
                [self logCardFlip:card andOtherCards:selectedCards];
            }
            self.score -= FLIP_COST;
        } else {
            [self logCardUnflip:selectedCards];
        }
        card.faceUp = !card.isFaceUp;
    }
}

- (void)setNumCardsMatch:(NSUInteger)numCardsMatch {
    if ([self mayChangeMode]) {
        if (numCardsMatch < 2) {
            numCardsMatch = 2;
        }
        _numCardsMatch = numCardsMatch;
    }
}

- (BOOL)mayChangeMode{
    return !self.gameStarted;
}

- (void)removeCardsAtIndexes:(NSIndexSet *)indexes
{
    [self.cards removeObjectsAtIndexes:indexes];
}

@end
