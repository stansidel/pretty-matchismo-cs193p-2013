//
//  SetCatdCollectionViewCell.h
//  Matchismo
//
//  Created by Stanislav Sidelnikov on 09/08/15.
//  Copyright © 2015 Stanford University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetCardView.h"

@interface SetCardCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet SetCardView *setCardView;

@end
