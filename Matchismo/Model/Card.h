//
//  Card.h
//  Matchismo
//
//  Created by stan on 23/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject

@property (strong, nonatomic) NSString * contents;

@property (nonatomic, getter=isFaceUp) BOOL faceUp;
@property (nonatomic, getter=isUnplayable) BOOL unplayable;

- (NSInteger)match:(NSArray *)otherCards;

@end
